package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.List;
import java.util.NoSuchElementException;

import org.apache.ibatis.session.SqlSession;

public class OrderDao {
  private OrderMapper mapper;

  public OrderDao(SqlSession sqlSession) {
    mapper = sqlSession.getMapper(OrderMapper.class);
  }

  public OrderRow create(InvoiceListRow invoiceList, OrderHistoryRow orderHistory) {
    OrderRow retval = new OrderRow(0);
    retval.setInvoiceList(invoiceList);
    retval.setOrderHistory(orderHistory);
    mapper.insert(retval);
    return retval;
  }

  public OrderRow getShallow(int orderId) {
    OrderRow retval = mapper.selectShallow(orderId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }

  public List<OrderRow> readList(OrderListRow orderList) {
    List<OrderRow> retval = mapper.readList(orderList);
    return retval;
  }

}
