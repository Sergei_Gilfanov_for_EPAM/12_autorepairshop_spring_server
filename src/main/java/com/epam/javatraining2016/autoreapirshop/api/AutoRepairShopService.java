package com.epam.javatraining2016.autoreapirshop.api;

import com.epam.javatraining2016.autoreapirshop.protocol.Client;
import com.epam.javatraining2016.autoreapirshop.protocol.Comment;
import com.epam.javatraining2016.autoreapirshop.protocol.Order;
import com.epam.javatraining2016.autoreapirshop.protocol.OrderHistoryRecord;

public interface AutoRepairShopService {
  public String getVersion();

  public int createOrder(int clientId, String commentText);

  public Order getOrder(int orderId);

  public Order[] getOrdersForClient(int clientId);

  public int changeOrderStatus(int orderId, String statusName, String commentText);

  public int createClient(String name);

  public Client getClient(int clientId);

  public Client[] getClients();

  public int createComment(int orderId, String commentText);

  public Comment getComment(int commentId);

  public OrderHistoryRecord[] getHistory(int id);

}
