package com.epam.javatraining2016.autoreapirshop.api.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.epam.javatraining2016.autoreapirshop.SessionFactory;
import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.autoreapirshop.dao.AutoRepairShopServiceDao;
import com.epam.javatraining2016.autoreapirshop.dao.ClientRow;
import com.epam.javatraining2016.autoreapirshop.dao.CommentRow;
import com.epam.javatraining2016.autoreapirshop.dao.InvoiceListRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderHistoryRecordCommentRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderHistoryRecordRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderHistoryRecordStatusChangeRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderHistoryRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderListRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderStatusRow;
import com.epam.javatraining2016.autoreapirshop.protocol.Client;
import com.epam.javatraining2016.autoreapirshop.protocol.Comment;
import com.epam.javatraining2016.autoreapirshop.protocol.Order;
import com.epam.javatraining2016.autoreapirshop.protocol.OrderHistoryRecord;
import com.epam.javatraining2016.autoreapirshop.protocol.OrderHistoryRecordComment;
import com.epam.javatraining2016.autoreapirshop.protocol.OrderHistoryRecordStatusChange;

@Component(value = "autoRepairShopService")
public class AutoRepairShopServiceImpl implements AutoRepairShopService {
  private static final Logger log = LoggerFactory.getLogger(AutoRepairShopServiceImpl.class);

  @Autowired
  private SessionFactory sessionFactory;

  public AutoRepairShopServiceImpl() {
    log.info("AutoRepairShopServiceImpl:default constructor");
  }

  @Override
  public String getVersion() {
    return "sergei_gilfanov@epam.com,2016-06-27";
  }

  @Override
  @Transactional
  public int createClient(String name) {
    log.info("createClient");
    ClientRow retval;
    try (AutoRepairShopServiceDao dao = sessionFactory.getDao()) {
      OrderListRow orderList = dao.orderList().create();
      retval = dao.client().create(name, orderList);
      dao.commit();
    }
    return retval.getId();
  }

  @Override
  @Transactional
  public Client getClient(int clientId) {
    log.info("getClient");
    ClientRow client;
    try (AutoRepairShopServiceDao dao = sessionFactory.getDao()) {
      client = dao.client().getShallow(clientId);
    }

    Client retval = new Client();
    retval.setId(client.getId());
    retval.setClientName(client.getClientName());
    retval.setOrderListId(client.getOrderList().getId());
    return retval;
  }

  @Override
  @Transactional
  public Client[] getClients() {
    log.info("getClients");

    List<Client> retval;
    try (AutoRepairShopServiceDao dao = sessionFactory.getDao()) {
      List<ClientRow> clients = dao.client().getAll();
      retval = new ArrayList<Client>(clients.size());
      for (ClientRow clientRow : clients) {
        Client client = new Client();
        client.setId(clientRow.getId());
        client.setClientName(clientRow.getClientName());
        client.setOrderListId(clientRow.getOrderList().getId());
        retval.add(client);
      }
    }
    return retval.toArray(new Client[0]);
  }

  @Override
  @Transactional
  public int createComment(int orderId, String commentText) {
    log.info("createComment");
    OrderHistoryRecordRow orderHistoryRecord;
    try (AutoRepairShopServiceDao dao = sessionFactory.getDao()) {
      OrderRow order = dao.order().getShallow(orderId);
      CommentRow comment = dao.comment().create(commentText);
      orderHistoryRecord = dao.orderHistoryRecordComment().create(order.getOrderHistory(), comment);
      dao.commit();
    }
    return orderHistoryRecord.getId();
  }

  @Override
  @Transactional
  public Comment getComment(int commentId) {
    log.info("getComment");
    CommentRow comment;
    try (AutoRepairShopServiceDao dao = sessionFactory.getDao()) {
      comment = dao.comment().get(commentId);
    }
    Comment retval = new Comment();
    retval.setId(comment.getId());
    retval.setCommentText(comment.getCommentText());
    return retval;
  }

  @Override
  @Transactional
  public int createOrder(int clientId, String commentText) {
    log.info("createOrder");
    OrderRow order;
    try (AutoRepairShopServiceDao dao = sessionFactory.getDao()) {
      ClientRow client = dao.client().getShallow(clientId);
      InvoiceListRow invoiceList = dao.invoiceList().create();
      OrderHistoryRow orderHistory = dao.orderHistory().create();
      order = dao.order().create(invoiceList, orderHistory);
      dao.orderList().add(client.getOrderList(), order);
      changeOrderStatusCommon(dao, order, "new", commentText);
      dao.commit();
    }
    return order.getId();
  }

  @Override
  @Transactional
  public Order getOrder(int orderId) {
    log.info("getComment");
    OrderRow order;
    OrderStatusRow status;
    try (AutoRepairShopServiceDao dao = sessionFactory.getDao()) {
      order = dao.order().getShallow(orderId);
      status = dao.orderStatus().getCurrentStatus(order);
    }

    Order retval = new Order();
    retval.setId(order.getId());
    retval.setStatus(status.getStatusName());
    return retval;
  }

  public OrderHistoryRecordRow changeOrderStatusCommon(AutoRepairShopServiceDao dao, OrderRow order,
      String statusName, String commentText) {
    OrderStatusRow status = dao.orderStatus().findByName(statusName);
    CommentRow comment = dao.comment().create(commentText);
    OrderHistoryRecordRow orderHistoryRecord =
        dao.orderHistoryRecordStatusChange().create(order.getOrderHistory(), status, comment);
    return orderHistoryRecord;
  }

  @Override
  @Transactional
  public int changeOrderStatus(int orderId, String statusName, String commentText) {
    OrderHistoryRecordRow orderHistoryRecord;
    try (AutoRepairShopServiceDao dao = sessionFactory.getDao()) {
      OrderRow order = dao.order().getShallow(orderId);
      orderHistoryRecord = changeOrderStatusCommon(dao, order, statusName, commentText);
      dao.commit();
    }
    return orderHistoryRecord.getId();
  }

  @Override
  @Transactional
  public Order[] getOrdersForClient(int clientId) {
    List<Order> retval;
    try (AutoRepairShopServiceDao dao = sessionFactory.getDao()) {
      ClientRow client = dao.client().getShallow(clientId);
      List<OrderRow> orders = dao.order().readList(client.getOrderList());
      retval = new ArrayList<Order>(orders.size());
      for (OrderRow orderRow : orders) {
        Order order = new Order();
        OrderStatusRow orderStatus = dao.orderStatus().getCurrentStatus(orderRow);
        order.setId(orderRow.getId());
        order.setStatus(orderStatus.getStatusName());
        retval.add(order);
      }
    }
    return retval.toArray(new Order[0]);
  }

  @Override
  public OrderHistoryRecord[] getHistory(int orderId) {
    List<OrderHistoryRecord> retval;
    try (AutoRepairShopServiceDao dao = sessionFactory.getDao()) {
      // Получаем отдельно список комментариев, список изменений статуса и сливаем их в один общий
      // список результата
      List<OrderHistoryRecordCommentRow> commentList =
          dao.orderHistoryRecordComment().findForOrderDeep(orderId);
      List<OrderHistoryRecordStatusChangeRow> statusChangeList =
          dao.orderHistoryRecordStatusChange().findForOrderDeep(orderId);
      retval = new ArrayList<OrderHistoryRecord>(commentList.size() + statusChangeList.size());

      // Добавляем в историю записи комментариев
      for (OrderHistoryRecordCommentRow commentRow : commentList) {
        Comment comment = new Comment();
        comment.setId(commentRow.getComment().getId());
        comment.setCommentText(commentRow.getComment().getCommentText());

        OrderHistoryRecordComment orderHisttoryRecordComment = new OrderHistoryRecordComment();
        orderHisttoryRecordComment.setId(commentRow.getId());
        orderHisttoryRecordComment.setComment(comment);

        retval.add(orderHisttoryRecordComment);
      }

      // Добавляем в историю записи изменения статуса
      for (OrderHistoryRecordStatusChangeRow statusChangeRow : statusChangeList) {
        Comment comment = new Comment();
        comment.setId(statusChangeRow.getComment().getId());
        comment.setCommentText(statusChangeRow.getComment().getCommentText());

        OrderHistoryRecordStatusChange orderHistoryRecordStatusChange =
            new OrderHistoryRecordStatusChange();
        orderHistoryRecordStatusChange.setId(statusChangeRow.getId());
        orderHistoryRecordStatusChange.setStatus(statusChangeRow.getOrderStatus().getStatusName());
        orderHistoryRecordStatusChange.setComment(comment);

        retval.add(orderHistoryRecordStatusChange);
      }
    }
    // Комментарии и изменения статуса могли идти вперемешку. Делаем историю
    // линейной, отсортировав ее по id записи
    retval.sort((a, b) -> ((Integer) a.getId()).compareTo(b.getId()));
    return retval.toArray(new OrderHistoryRecord[0]);
  }
}
