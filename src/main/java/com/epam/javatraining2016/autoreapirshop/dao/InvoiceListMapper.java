package com.epam.javatraining2016.autoreapirshop.dao;

public interface InvoiceListMapper {
  void insert(InvoiceListRow invoiceList);

  InvoiceListRow select(int orderListId);
}
