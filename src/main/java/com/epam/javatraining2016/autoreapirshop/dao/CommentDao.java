package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.apache.ibatis.session.SqlSession;

public class CommentDao {
  private CommentMapper mapper;

  public CommentDao(SqlSession sqlSession) {
    mapper = sqlSession.getMapper(CommentMapper.class);
  }

  public CommentRow create(String commentText) {
    CommentRow retval = new CommentRow(0);
    retval.setCommentText(commentText);
    mapper.insert(retval);
    return retval;
  }

  public CommentRow get(int commentId) {
    CommentRow retval = mapper.select(commentId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
