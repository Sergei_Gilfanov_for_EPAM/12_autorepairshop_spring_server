package com.epam.javatraining2016.autoreapirshop.dao;

public interface OrderHistoryRecordMapper {
  void insert(OrderHistoryRecordRow retval);

  OrderHistoryRecordRow selectShallow(int orderHistoryRecordId);
}
