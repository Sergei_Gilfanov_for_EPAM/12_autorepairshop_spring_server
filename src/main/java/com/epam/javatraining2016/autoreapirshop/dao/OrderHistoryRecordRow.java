package com.epam.javatraining2016.autoreapirshop.dao;

public class OrderHistoryRecordRow extends Row {
  private OrderHistoryRow orderHistory;

  OrderHistoryRecordRow(Integer id) {
    super(id);
  }

  public OrderHistoryRow getOrderHistory() {
    return orderHistory;
  }

  public void setOrderHistory(OrderHistoryRow orderHistory) {
    this.orderHistory = orderHistory;
  }
}
