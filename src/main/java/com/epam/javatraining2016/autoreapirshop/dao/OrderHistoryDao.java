package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.apache.ibatis.session.SqlSession;

public class OrderHistoryDao {
  private OrderHistoryMapper mapper;

  public OrderHistoryDao(SqlSession sqlSession) {
    mapper = sqlSession.getMapper(OrderHistoryMapper.class);
  }

  public OrderHistoryRow create() {
    OrderHistoryRow retval = new OrderHistoryRow(0);
    mapper.insert(retval);
    return retval;
  }

  public OrderHistoryRow get(int orderHistoryId) {
    OrderHistoryRow retval = mapper.select(orderHistoryId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
