package com.epam.javatraining2016.autoreapirshop.dao;

public interface OrderHistoryMapper {
  void insert(OrderHistoryRow orderHistory);

  OrderHistoryRow select(int orderHistoryId);
}
